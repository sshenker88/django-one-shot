from django.shortcuts import render, redirect, get_object_or_404
from .models import TodoList, TodoItem
from .forms import TodoListForm, ListItemForm

# Create your views here.

def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {
        "list_object":lists,
    }
    return render(request, "lists/todos.html", context)




def list_details(request, id):
    items = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_detail":items,
    }
    return render(request, "lists/todo_list_detail.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            newlist = form.save()
            return redirect("todo_list_detail", id=newlist.id)
    else:
        form = TodoListForm()
        context = {
        "form": form,
        }
        return render(request, "lists/todo_list_create.html", context)

def todo_list_update(request, id):
    curr_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance = curr_list)
        if form.is_valid():
            newlist = form.save()
            return redirect("todo_list_detail", id=newlist.id)
    else:
        form = TodoListForm(request.POST, instance = curr_list)
        context = {
        "form": form,
        "current_list": curr_list,
        }
        return render(request, "lists/todo_list_edit.html", context)

def todo_list_delete(request, id):
    deleted_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        deleted_list.delete()
        print("I did something find me")
        return redirect("todo_list_list")

    return render(request, "lists/todo_list_delete.html")

def todo_item_create(request):
    if request.method == "POST":
        form = ListItemForm(request.POST)
        if form.is_valid():
            newlist = form.save()
            return redirect("todo_list_detail", id=newlist.list.id)
    else:
        form = ListItemForm()
        context = {
        "form": form,
        }
        return render(request, "lists/todo_item_create.html", context)


def todo_item_update(request, id):
    curr_item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = ListItemForm(request.POST, instance = curr_item)
        if form.is_valid():
            newlist = form.save()
            return redirect("todo_list_detail", id=newlist.list.id)
    else:
        form = ListItemForm(request.POST, instance = curr_item)
        context = {
        "form": form,
        "current_item": curr_item,
        }
        return render(request, "lists/todo_item_edit.html", context)
