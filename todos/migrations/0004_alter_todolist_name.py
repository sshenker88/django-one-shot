# Generated by Django 4.1.5 on 2023-01-18 23:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("todos", "0003_alter_todoitem_task"),
    ]

    operations = [
        migrations.AlterField(
            model_name="todolist",
            name="name",
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
